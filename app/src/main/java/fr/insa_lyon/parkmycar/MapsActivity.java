package fr.insa_lyon.parkmycar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

//import io.nlopez.clusterer.Clusterer;

import static android.widget.Toast.LENGTH_LONG;
import static fr.insa_lyon.parkmycar.R.id.button2;
import static fr.insa_lyon.parkmycar.R.id.map;
import static fr.insa_lyon.parkmycar.R.id.rayon;
import static fr.insa_lyon.parkmycar.R.id.textView;
import static java.lang.Thread.sleep;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, PlaceSelectionListener {

    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    //Acces a la BDD Firebase :
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mPlaceRef = mRootRef.child("Place");
    TextView mTextView;
    Button mButtonOptiPark;
    EditText mRayonRecherche;
    SeekBar mTempsMarche;
    //On cree une map avec tous les marqueurs
    private HashMap<String,Marker> markerMap = new HashMap<String,Marker>();
    private FusedLocationProviderClient mFusedLocationClient;
    GoogleApiClient mGoogleApiClient;
    private double rayonRecherche = 1.0;
    private double tempsMarcheInitial;
    LocationManager locationManager;
    private LatLng locationChosen = null;

    // API Places
    private static final String LOG_TAG = "PlaceSelectionListener";
    private static final int REQUEST_SELECT_PLACE = 1000;

    //HeatMap
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;
    private HashMap<String, DataSet> mLists = new HashMap<String, DataSet>();
    private String finalArray ="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_maps);
        //Check si la localisation est allumée :
        isLocationEnabled();
        if(!isLocationEnabled()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
            builder.setTitle("La localisation est requise pour OptiPark")
                    .setMessage("Souhaitez vous ouvrir les paramètres ?")
                    .setPositiveButton("Oui",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton("non",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish(); //On arrête l'activité
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);

        mTextView = (TextView) findViewById(textView);

        tempsMarcheInitial = 0;
        mTextView.setText("Me garer à moins de 1 minute");

        mButtonOptiPark = (Button) findViewById(button2);
        mTempsMarche = (SeekBar) findViewById(rayon);
        mTempsMarche.setProgress((int)tempsMarcheInitial); //On initialise la seekbar
        mTempsMarche.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + 1;
                tempsMarcheInitial = progress;
                if (progress == 0){
                    mTextView.setText("Me garer à moins de 1 minute");
                    progress = 1;
                }
                if(progress == 1){
                    mTextView.setText("Me garer à moins de "+progress+" minute");
                }
                else{
                    mTextView.setText("Me garer à moins de "+progress+" minutes");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //API Places
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);
        autocompleteFragment.setHint("Adresse de destination");

    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mLists.put("Places", new DataSet(readItems(R.raw.places)));
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Problème mLists", LENGTH_LONG).show();
        }
        mButtonOptiPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog = ProgressDialog.show(MapsActivity.this, "",
                        "Veuillez patienter, nous recherchons la zone de stationnement où vous aurez le plus de chance de pouvoir vous garer", true);
                //On fait la requête pour obtenir les infos sur la place (latitude et longitude)
                String url = "http://jheizmann.pythonanywhere.com"; //URL vers le serveur web

                //Request a string response from the URL resource
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                String latLng = response.toString();
                                //ICI IL FAUDRA PARSER LA REPONSE
                                System.out.println("SERVER RESPONSE = "+latLng);
                                if(latLng.split(";").length == 1){
                                    Toast.makeText(getApplicationContext(),"Vous n'avez pas de zones de stationnement à proximité de cette localisation",Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }else {
                                    //La reponse est de la forme lat;lng
                                    String[] coord = latLng.split(";");
                                    final String lat = coord[0];
                                    final String lng = coord[1];
                                    int rayonRechercheServeur = (int) Float.parseFloat(coord[2]);
                                    //Si la place est à plus de 2.5 kms
                                    if(rayonRechercheServeur>25.0){
                                        Toast.makeText(getApplicationContext(),"Vous n'avez pas de zones de stationnement à proximité de cette localisation",Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                    }else{
                                        System.out.println("tempsMarcheInitial = "+tempsMarcheInitial+"rayonRechercheServeur = "+rayonRechercheServeur);
                                        if(tempsMarcheInitial<rayonRechercheServeur){
                                            //Si on a changé le rayon alors on met l'alert dialog :
                                            AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                                            builder.setTitle("Information")
                                                    .setMessage("Le temps de marche à été augmenté à "+coord[2]+" minutes car les zones de stationnement plus proches n'étaient pas disponible, souhaitez vous continuer ?")
                                                    .setPositiveButton("oui",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                    //On démarre google Maps pour aller vers la place :
                                                                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
                                                                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                                                    mapIntent.setPackage("com.google.android.apps.maps");
                                                                    startActivity(mapIntent);
                                                                }
                                                            })
                                                    .setNegativeButton("non", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.cancel();
                                                        }
                                            });
                                            AlertDialog alert = builder.create();
                                            alert.show();
                                        }else{
                                            //Sinon on démarre directement maps
                                            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
                                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                            mapIntent.setPackage("com.google.android.apps.maps");
                                            startActivity(mapIntent);
                                        }
                                        mTempsMarche.setProgress(rayonRechercheServeur);
                                        //Ajouter une alertDialog pour dire que on a changé le slider
                                        System.out.println("LatitudePlace = " + lat + " LongitudePlace = " + lng);

                                        dialog.dismiss();


                                    }
                                }
                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Oops! That didn't work!, server error");
                        Toast.makeText(getApplicationContext(),"Erreur du serveur"+error.toString(),Toast.LENGTH_SHORT).show();

                    }})
                {
                    @SuppressLint("MissingPermission")

                    //On envoie au serveur notre position avec getBody :
                    @Override
                    public byte[] getBody(){
                        double latitude = 45.7659432901;
                        double longitude = 4.84926911184; //Valeurs pas defaut
                        Task getLocation =  mFusedLocationClient.getLastLocation().addOnSuccessListener(MapsActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    System.out.println("On envoie notre position");
                                }
                            }
                        });

                        //Comme on a des threads en parrallele, on attend d'avoir récupéré la latiutde et la longitude avant de continuer
                        while(getLocation.isComplete() == false){
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        Location currentLocation = (Location) getLocation.getResult();
                        if(locationChosen != null){
                            latitude = locationChosen.latitude;
                            longitude = locationChosen.longitude;
                            //ICI METTRE LE CODE POUR AVOIR LE TEMPS DE TRAJET

                        }else{
                            latitude = currentLocation.getLatitude(); //MAJ latitude avec latitude actuelle
                            longitude = currentLocation.getLongitude(); //MAJ longitude avec longitude actuelle
                        }

                        System.out.println("RAYON MARCHE ENVOYE = "+tempsMarcheInitial/10);
                        String str ="";
                        System.out.println("DUREE ENVOYEE = "+finalArray);

                        str = latitude+";"+longitude+";"+tempsMarcheInitial/10+";"+finalArray;

                        System.out.println("STR = "+str);
                        //On envoie au serveur :
                        return str.getBytes();
                    }
                    public String getBodyContentType()
                    {
                        return "application/json; charset=utf-8";
                    }
                };
                //Instantiate the RequestQueue and add the request to the queue
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                RetryPolicy policy = new DefaultRetryPolicy(40000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                stringRequest.setRetryPolicy(policy);
                queue.add(stringRequest);

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }

        //Zoom sur la localisation actuelle :
        Location locationCt;
        Location locationCtNetwork;
        LocationManager locationManagerCt = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationCt = locationManagerCt.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        locationCtNetwork = locationManagerCt.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if(locationCt!=null){
            LatLng myLatLng = new LatLng(locationCt.getLatitude(),locationCt.getLongitude());
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(myLatLng,15));
        }else if(locationCtNetwork !=null){
            LatLng myLatLngNet = new LatLng(locationCtNetwork.getLatitude(),locationCtNetwork.getLongitude());
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(myLatLngNet,15));
        }

        try {
            mLists.put("Places", new DataSet(readItems(R.raw.places)));
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Problème mLists", LENGTH_LONG).show();
        }

        if (mProvider == null) {
            mProvider = new HeatmapTileProvider.Builder().weightedData(
                    mLists.get("Places").getData()).build();
            mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
        } else {
            mProvider.setWeightedData(mLists.get("Places").getData());
            mOverlay.clearTileCache();
        }
    }

    // Gestion de l'API Places

    @Override
    public void onPlaceSelected(Place place) {
        Log.i(LOG_TAG, "Place selectionnée " + place.getName());
        //Toast.makeText(this, "You selected the place : " + place.getName() + " with the coordinates " + place.getLatLng(),Toast.LENGTH_LONG).show();
        locationChosen = place.getLatLng();
        mMap.addMarker(new MarkerOptions().position(locationChosen).title("Your Place"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(locationChosen));

        // Calcul du temps de trajet


            Thread thread = new Thread() {
                @Override
                public void run() {
                    if (ContextCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        Task getLocation = mFusedLocationClient.getLastLocation().addOnSuccessListener(MapsActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    System.out.println("On envoie notre position");
                                }
                            }
                        });

                        //Comme on a des threads en parrallele, on attend d'avoir récupéré la latiutde et la longitude avant de continuer
                        while (getLocation.isComplete() == false) {
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        Location currentLocation = (Location) getLocation.getResult();

                        String APIKeyServer = "AIzaSyCci_RdoE-Dvp-Ei7qkh1tsNXOYsBj__rQ";
                        String urlRequest = "https://maps.googleapis.com/maps/api/directions/json?origin="
                                + String.valueOf(currentLocation.getLatitude()) + "," + String.valueOf(currentLocation.getLongitude()) + "&destination=" + locationChosen.latitude + "," + locationChosen.longitude + "&key=" + APIKeyServer;
                        System.out.println("URL = " + urlRequest);

                        HttpHandler sh = new HttpHandler();

                        // Making a request to url and getting response

                        String jsonStr = sh.makeServiceCall(urlRequest);
                        String TAG = MapsActivity.class.getSimpleName();
                        Log.e(TAG, "Response from url: " + jsonStr);
                        //System.out.println("Response from url = " + jsonStr);

                        if (jsonStr != null) {
                            try {
                                JSONObject jsonObj = new JSONObject(jsonStr);

                                // Getting JSON Array node
                                JSONArray routes = jsonObj.getJSONArray("routes");

                                System.out.println("Duration length = " + routes.length());

                                JSONObject c = routes.getJSONObject(0);
                                String duration = c.getString("legs");
                                System.out.println("Duration of traject = " + duration);

                                String[] splitArray;
                                String[] splitArray2;
                                splitArray = duration.split(",");
                                splitArray2 = splitArray[1].split(":");
                                finalArray = splitArray2[1].substring(0, splitArray2[1].length() - 1);
                                System.out.println("Final duration = " + finalArray);

                                /* FINAL ARRAY IS THE FINAL TIME */

                            } catch (final JSONException e) {
                                //Log.e(TAG, "Json parsing error: " + e.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Toast.makeText(getApplicationContext(),
                                        //        "Json parsing error: " + e.getMessage(),
                                        //        Toast.LENGTH_LONG)
                                        //        .show();
                                    }
                                });

                            }
                        } else {
                            Log.e(TAG, "Couldn't get json from server.");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(),
                                            "Couldn't get json from server. Check LogCat for possible errors!",
                                            Toast.LENGTH_LONG)
                                            .show();
                                }
                            });

                        }
                    }
                }
            };

            thread.start();

        }



    @Override
    public void onError(Status status) {
        Log.e(LOG_TAG, "onError: Status = " + status.toString());
        Toast.makeText(this, "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                this.onError(status);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Gestion des permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                //On regarde si on a les permissions ou pas
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Il faut la localisation ...", LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    protected boolean isLocationEnabled(){
        String le = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(le);
        if(!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && !locationManager.isProviderEnabled((LocationManager.GPS_PROVIDER))){
            return false;
        } else {
            return true;
        }
    }

    protected ArrayList<WeightedLatLng> readItems(int resource) throws JSONException {
        ArrayList<WeightedLatLng> list = new ArrayList<>();
        InputStream inputStream = getResources().openRawResource(resource);
        String json = new Scanner(inputStream).useDelimiter("\\A").next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double lat = object.getDouble("lat");
            double lng = object.getDouble("lng");
            double weight = object.getDouble("weight");
            LatLng position = new LatLng(lat, lng);
            list.add(new WeightedLatLng(position, weight));
        }
        return list;
    }

    public class DataSet {
        private ArrayList<WeightedLatLng> mDataset;

        public DataSet(ArrayList<WeightedLatLng> dataSet) {
            this.mDataset = dataSet;
        }

        public ArrayList<WeightedLatLng> getData() {
            return mDataset;
        }

    }

}
