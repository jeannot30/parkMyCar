package fr.insa_lyon.parkmycar;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class ParkMyCar extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_park_my_car);

        Button afficher_carte = (Button) findViewById(R.id.afficher_carte);
        Button afficher_infos = (Button) findViewById(R.id.afficher_infos);
        final Intent map = new Intent(this, fr.insa_lyon.parkmycar.MapsActivity.class);
        final Intent info = new Intent(this,fr.insa_lyon.parkmycar.infoActivity.class);
        afficher_carte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(map);
            }
        });

        afficher_infos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(info);
            }
        });
    }
}
